# Presentation - FW Development in Rust

Links from the presentation on FW development in Rust

Here's a list of good introductory resources for embedded Rust:

- [Awesome Embedded Rust](https://github.com/rust-embedded/awesome-embedded-rust): List of crates (packages) for embedded devices.
- [Embedded Rust Book](https://docs.rust-embedded.org/book/intro/index.html): Introduction to Rust on embedded devices.
- [The Rust Programming Language Book](https://doc.rust-lang.org/book/title-page.html): Online documentation of Rust in general.
- [Rustonomicon](https://doc.rust-lang.org/nomicon/ffi.html): Online book on unsafe Rust code. Requires prior knowledge of Rust.
- [Oreilly Programming Rust (printed book)](https://www.oreilly.com/library/view/programming-rust-2nd/9781492052586/): Book on Rust with many comparisons to C++.
- [High Assurance Rust](https://highassurance.rs/): _Developing Secure and Robust Software._
- [crates.io](https://crates.io): Rust package registry. `no_std` crates are suitable for embedded devices (no memory allocation, no standard library).

Packages (crates) typically all have the same structure of documentation. Let's say looking into RTIC, you will find.

1. an overview of the crate on [crates.io](https://crates.io/crates/cortex-m-rtic).
2. a repository containing the source-code e.g. on [GitHub](https://github.com/rtic-rs/cortex-m-rtic).
3. an API documentation on [docs.rs](https://rtic.rs/stable/api/rtic/index.html).
4. and sometimes a user level guide called [book](https://rtic.rs/1/book/en/).


## Espresso Machine Hardware (Slide 3)

- [Hardware Design files](https://gitlab.com/bern-rtos/demo/espresso-machine/hardware)
- [Espresso Machine Firmware](https://gitlab.com/bern-rtos/demo/espresso-machine/firmware)
- Documentation of machine modifications: Will be published at the end of the master thesis


## Board Support Package (Slide 5..6)

- [Embedded HAL](https://docs.rs/embedded-hal/latest/embedded_hal/): Traits for HW interfaces such as SPI, I2C, UART, GPIO, etc. These are implemented in the HAL and can be use by platform agnostic device drivers.
- [stm32f7xx-hal](https://github.com/stm32-rs/stm32f7xx-hal): Example of HAL. See [Awesome Embedded Rust](https://github.com/rust-embedded/awesome-embedded-rust) for a list of all HAL implementations.
- [STM32 Peripheral Access Crates](https://github.com/stm32-rs/stm32-rs): If you want to access the registers of microcontroller you can use the PAC.
- [Embedded Rust Book](https://docs.rust-embedded.org/book/intro/index.html): The embedded Rust book gives a introduction on to set-up the Rust toolchain and how to use the tools available.


## Device Driver | Embedded HAL (Slide 7..9)

- [List of driver crates](https://github.com/rust-embedded/awesome-embedded-rust#driver-crates): Check out the implementation of existing driver crates.
- [Rust book: writing tests](https://doc.rust-lang.org/book/ch11-01-writing-tests.html): Chapter on how to test Rust software.


## Traits (Slide 10)

- [Rust book: traits](https://doc.rust-lang.org/book/ch10-02-traits.html): Chapter on traits.


## GUI (Slide 11..12)

- [Rustonomicon: FFI](https://doc.rust-lang.org/nomicon/ffi.html): Chapter on foreign function interfaces, i.e. calling C from Rust
- [`cc`](https://crates.io/crates/cc): Crate used to compile C/C++ code from cargo.
- [`bindgen`](https://crates.io/crates/bindgen): Crate that generates Rust bindings from C header files.
- [`lvgl`](https://lvgl.io/): Lightweight C graphics library.
- [`lvgl-rs`](https://crates.io/crates/lvgl): Rust binding for LVGL.


## Communication Stacks (Slide 13)

- [`smoltcp`](https://crates.io/crates/smoltcp): TCP/IP stack without heap allocation.
- [`usb-device`](https://crates.io/crates/usb-device): USB device crate.



## RTOS & RT-Framework (Slide 14)

- [RTIC](https://crates.io/crates/cortex-m-rtic): Real-Time Interrupt-driven Concurrency framework.
- [Tock OS](https://www.tockos.org/): _An embedded operating system designed for running multiple concurrent, mutually distrustful applications on low-memory and low-power microcontrollers._
- [FreeRTOS-rust](https://github.com/lobaro/FreeRTOS-rust): FreeRTOS wrapper.
- [Bern RTOS](https://bern-rtos.org/): A real-time operating system for microcontrollers written in Rust. Uses processes to isolate critical parts of the software. Prevents stack overflow with the memory protection unit on ARM Cortex-M CPUs.



## Ownership & Lifetime (Slide 15)

- [Rust book: Ownership](https://doc.rust-lang.org/book/ch04-00-understanding-ownership.html): Chapter on ownership concept.
- [Rustonomicon: Ownership & Lifetime](https://doc.rust-lang.org/nomicon/ownership.html): Chapter on ownership and lifetime.
- [Graphical depiction of ownership and borrowing in Rust](https://rufflewind.com/2017-02-15/rust-move-copy-borrow)

---

## Macros (Slide 16..18)

- [Rust book: Macros](https://doc.rust-lang.org/book/ch19-06-macros.html): Chapter on Rusts macros in general.
- [Rust Reference: Procedural Macros](https://doc.rust-lang.org/reference/procedural-macros.html): Chapter on procedural macros. A procedural macro will call your code with the macro inputs, which you'll then use to generate any Rust code you can imagine.
- [`serde`](https://crates.io/crates/serde): _Serde is a framework for serializing and deserializing Rust data structures efficiently and generically._
- [`postcard`](https://crates.io/crates/postcard): _Postcard is a no_std crate focused serializer and deserializer for Serde._
- [`sfsm`](https://crates.io/crates/sfsm): _Static state machine generator for no_std and embedded environments_
- [bern-register-generator](): Not published yet.



## Standard library (Slide 19)

- [`std`](https://doc.rust-lang.org/std/): Standard library documentation.



## Development tools (Slide 20..22)

- [Cargo book](https://doc.rust-lang.org/cargo/): Documentation on Cargo. The Rust package manager. The following presentation material is from the cargo book:
    - Build commands
    - Project structure
    - Specifying dependencies
    - Custom registries
- Visual Studio Code
    - [Cortex-M Quickstart](https://github.com/rust-embedded/cortex-m-quickstart): The Cortex-M quickstart template already contains a VS Code debug config.
    - [VS Code: Cortex-M Debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug): Plugin to debug ARM Cortex-M chips.
    - [VS Code: Rust Analyzer](https://code.visualstudio.com/docs/languages/rust): Rust language support plugin.
- [IntelliJ CLion](https://www.jetbrains.com/rust/): Rust setup for CLion
    - To use the embedded debugger you'll have to wrap the cargo build command in CMake script (`add_custom_target`)
- [`cargo-license`](https://crates.io/crates/cargo-license): Creates a list of the licenses from all dependendencies.
- [`cargo-audit`](https://crates.io/crates/cargo-audit): Searches dependencies for reported security vulnerabilities.


## Rust Compiler Release Model (Slide 24..25)

- [Rust book: How Rust is Made](https://doc.rust-lang.org/book/appendix-07-nightly-rust.html): Rust release model.
- [Rust RFC book](https://rust-lang.github.io/rfcs/): Online book containing all implemented and requested features. Here you'll find the link to tracking issues, documenting the progress of the feature.


## Efficiency (Slide 26..27)

The flash size comparison is based on the espresso machine firmware with the following features:
- Machine control (switches as inputs, valves and boiler as outputs)
- Temperature measurement with RTD sensors and an external ADC (AD7124-8)
- Data logging via a TCP connection
- Bern RTOS kernel
The UI was excluded from the build, because the flash size would have been too large (>2MB) in debug mode.

Here are the sizes sorted by crate:

|   | Debug | Debug (defmt) | Debug opt. | Debug opt. (defmt) | Release | Release (defmt) |
|---|-------|-----------------|------------|--------------------|---------|-----------------|
| **Application** | 4.9  | 3.9  | 4.9  | 3.9  | 11.6  | 11.2 |
| **core** | 46.5  | 23.6  | 50.6  | 28.0  | 6.0  | 5.1 |
| **BSP** | 184.8  | 184.8  | 11.2  | 11.2  | 9.9  | 9.9 |
| **TCP/IP Stack** | 107.4  | 107.4  | 42.3  | 42.3  | 7.9  | 7.9 |
| **HAL** | 55.0  | 54.9  | 8.0  | 8.0  | 0.2  | 0.2 |
| **Device Driver** | 19.8  | 19.8  | 15.6  | 15.6  | 0.7  | 0.7 |
| **RTOS Kernel** | 58.9  | 52.7  | 14.4  | 11.7  | 8.3  | 7.6 |
| **Other** | 76.9  | 61.5  | 42.3  | 34.1  | 10.5  | 6.2 |
| **Total** | **554.2**  | **508.6**  | **189.3**  | **154.8**  | **55.0**  | **48.7** |

All sizes are in kB.

Rusts string formatting generates large binaries. To combat that you can use
- [`defmt`](https://crates.io/crates/defmt): _defmt ("de format", short for "deferred formatting") is a highly efficient logging framework that targets resource-constrained devices, like microcontrollers._ Formats data on the computer instead of the microntroller and transports unformatted values in binary.
- [`ufmt`](https://crates.io/crates/ufmt): Smaller and faster alternative to `core::fmt`. Still formats strings on the microcontroller (e.g. you want to output data on serial command line).

Code optimization:
- [Embedded Rust book: Optimization](https://docs.rust-embedded.org/book/unsorted/speed-vs-size.html): Types of optimizations with the Rust compiler.
- To optimize the dependencies of debug build you can add a new section to your `Cargo.toml`:
    ```toml
    [profile.dev.package."*"]  # all dependencies in debug mode
    codegen-units = 1   # only one compile thread, results in better optimizations
    opt-level = "s"     # opt. for size
    ```
